Summary

(Da el resumen del Issue)

Steps to reproduce

(Indica los pasos para reproducir el bug)

What is the correct behavior?

What is the expected behavior?